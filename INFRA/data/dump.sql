-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: apiGoodFood
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `commande`
--

CREATE DATABASE IF NOT EXISTS apiGoodFood;
USE apiGoodFood ;

DROP TABLE IF EXISTS `commande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commande` (
  `id` int NOT NULL AUTO_INCREMENT,
  `num_siret_rest_id` int DEFAULT NULL,
  `reservation_id` int DEFAULT NULL,
  `price_global` double NOT NULL,
  `valid_commande` tinyint(1) NOT NULL,
  `satisfaction_commande` int DEFAULT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6EEAA67D735387D5` (`num_siret_rest_id`),
  KEY `IDX_6EEAA67DB83297E7` (`reservation_id`),
  CONSTRAINT `FK_6EEAA67D735387D5` FOREIGN KEY (`num_siret_rest_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_6EEAA67DB83297E7` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commande`
--

LOCK TABLES `commande` WRITE;
/*!40000 ALTER TABLE `commande` DISABLE KEYS */;
/*!40000 ALTER TABLE `commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_rest_country` int NOT NULL,
  `ca_country` double NOT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (81,'Bosnia and Herzegovina',765198,2.515273,'1982-02-28 14:39:28','2017-02-20 11:52:13'),(82,'Paraguay',31114170,291309.49840628,'1996-10-22 16:03:14','1980-07-02 13:59:52'),(83,'Seychelles',804101165,591.04047,'1981-06-06 21:08:53','1998-04-30 16:01:58'),(84,'British Virgin Islands',596,192.6632,'1976-10-02 10:18:38','1987-08-18 05:54:43'),(85,'Haiti',9415781,52069.506,'2016-07-08 20:41:17','2010-12-05 22:52:35'),(86,'Vanuatu',84965632,4669886,'2006-08-02 19:29:32','1984-07-17 13:19:21'),(87,'Timor-Leste',899666135,8183613.0357,'1979-10-19 08:22:05','2003-04-15 19:09:22'),(88,'Swaziland',15270,0.6,'1972-10-30 20:16:10','2011-11-05 02:38:28'),(89,'Tanzania',90246,169.975758,'2017-03-29 16:04:50','2007-11-06 17:41:51'),(90,'Italy',6325790,28.97604,'1996-02-28 13:11:13','2003-05-15 12:16:21');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20220308102402','2022-04-09 13:54:10',1080),('DoctrineMigrations\\Version20220407134054','2022-04-09 13:54:11',411),('DoctrineMigrations\\Version20220409132502','2022-04-09 13:54:12',71),('DoctrineMigrations\\Version20220409134513','2022-04-09 13:54:12',140),('DoctrineMigrations\\Version20220409135306','2022-04-09 13:54:12',65),('DoctrineMigrations\\Version20220601113532','2022-06-03 15:47:22',72),('DoctrineMigrations\\Version20220603154724','2022-06-07 17:40:23',106),('DoctrineMigrations\\Version20220606191143','2022-06-07 17:40:23',65);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listcommandes`
--

DROP TABLE IF EXISTS `listcommandes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `listcommandes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `commande_id` int DEFAULT NULL,
  `menu_id` int DEFAULT NULL,
  `restaurant_id` int DEFAULT NULL,
  `reservation_id` int DEFAULT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_65795E2CA76ED395` (`user_id`),
  KEY `IDX_65795E2C82EA2E54` (`commande_id`),
  KEY `IDX_65795E2CCCD7E912` (`menu_id`),
  KEY `IDX_65795E2CB1E7706E` (`restaurant_id`),
  KEY `IDX_65795E2CB83297E7` (`reservation_id`),
  CONSTRAINT `FK_65795E2C82EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `commande` (`id`),
  CONSTRAINT `FK_65795E2CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_65795E2CB1E7706E` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurant` (`id`),
  CONSTRAINT `FK_65795E2CB83297E7` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  CONSTRAINT `FK_65795E2CCCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listcommandes`
--

LOCK TABLES `listcommandes` WRITE;
/*!40000 ALTER TABLE `listcommandes` DISABLE KEYS */;
/*!40000 ALTER TABLE `listcommandes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_menu` double NOT NULL,
  `id_country_menu` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `type_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_rank_menu` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7D053A933AB3E0E4` (`name_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (64,'MENU 1',72.11,'ZW','2004-06-27 00:09:51','1996-05-02 13:57:13','ITALIEN',0),(65,'MENU 2',80.25,'LK','1994-01-19 08:07:40','2009-01-02 21:01:18','ASIATIQUE',1),(66,'MENU 3',61.57,'GF','2005-11-04 05:03:35','2002-11-20 15:02:07','ASIATIQUE',2),(67,'MENU 4',44.13,'SS','1982-01-25 13:02:17','1984-02-26 11:28:05','FRANCAIS',3),(68,'MENU 5',12.79,'DM','1995-09-21 09:40:34','2007-03-15 09:43:27','POISSON',4),(69,'MENU 6',64.82,'BT','1986-05-04 01:17:42','1983-09-14 02:57:55','ITALIEN',5),(70,'MENU 7',41.29,'LT','1971-04-03 07:19:08','1987-01-24 04:12:41','ITALIEN',6),(71,'MENU 8',20.33,'FR','1990-05-13 05:15:57','2005-08-21 23:07:01','POISSON',7),(72,'MENU 9',71.47,'TV','1991-08-04 18:36:20','1972-07-16 12:38:53','ITALIEN',8),(73,'MENU 10',63.72,'LS','2016-05-29 12:19:42','1992-05-27 18:27:21','ITALIEN',9);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `number_customers` int NOT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse_restaurant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant`
--

DROP TABLE IF EXISTS `restaurant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `num_siret_rest` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse_rest` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_place_rest` int NOT NULL,
  `time_open_rest` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `time_close_rest` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `number_staff_rest` int NOT NULL,
  `number_visit_rest` int NOT NULL,
  `country_rest` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ranknumber` int NOT NULL,
  `satisfaction_rate_rest` double NOT NULL,
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `ca_rest` double NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_EB95123F40783D37` (`num_siret_rest`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant`
--

LOCK TABLES `restaurant` WRITE;
/*!40000 ALTER TABLE `restaurant` DISABLE KEYS */;
INSERT INTO `restaurant` VALUES (43,'80295478500021','843 Purdy Parks Apt. 310\nMarcelofurt, NV 14111',46104,'1984-12-24 09:57:31','1993-07-14 02:09:09',5,774636453,'BZ',0,74.84,'2011-05-22 19:28:28','2018-09-27 02:46:36',28,'Jaidaland'),(44,'80295478500022','306 Estelle Point Suite 563\nTerryborough, MS 27602-3476',20888,'1992-05-04 22:02:48','1996-10-04 22:16:36',88,13,'AG',1,81.9,'2019-01-08 16:31:07','2020-06-10 01:21:16',14274542,'West Randy'),(45,'80295478500023','93081 Theresia Tunnel\nWest Biankaview, WA 12215',52139074,'1995-07-15 07:46:05','1981-01-04 19:30:11',8574,6345,'PS',2,29.65,'1990-03-21 12:47:57','1973-07-17 18:38:07',740381893,'Powlowskibury'),(46,'80295478500024','789 Mosciski Mews\nKamillestad, WA 13443',5,'2012-09-18 19:44:25','1987-10-31 06:25:17',549023,57515,'RW',3,93.86,'1982-12-27 17:54:04','1981-02-21 10:51:31',807,'Pfefferview'),(47,'80295478500025','2442 Trevion Roads Suite 464\nNew Otilia, WY 56596-5700',149759,'1998-03-28 08:50:58','1995-11-12 11:41:42',4967256,417,'SE',4,10.35,'2001-09-10 15:06:46','2009-11-24 21:57:49',23135,'East Jakobton'),(48,'80295478500026','995 Vernie Fall\nVonburgh, LA 92975-3172',37694257,'2002-11-01 07:09:01','2001-08-28 10:06:21',7422995,940302,'EE',5,29.23,'1980-08-01 07:04:12','2012-04-29 21:38:14',430,'Emiliobury'),(49,'80295478500027','7277 Nils Freeway Suite 955\nWest Rosario, VT 18424',169519,'1984-12-20 02:29:12','2004-12-23 08:04:39',123867018,4682,'SG',6,94.26,'1984-06-08 11:09:54','1973-11-10 23:43:50',6,'Armstrongland'),(50,'80295478500028','7130 Schumm Walks\nBennyburgh, UT 85800',4184137,'2017-12-14 01:22:34','1974-09-19 23:35:09',60563,404344,'CX',7,12.18,'2016-04-06 01:55:41','1998-08-17 01:39:08',5618301,'South Orinfort'),(51,'80295478500029','242 Trinity Center\nFaheyton, LA 42001-7851',24860,'2021-05-16 19:27:21','1970-06-25 14:55:44',190,514,'NL',8,70.65,'2012-02-18 03:54:22','1989-02-16 20:56:52',36646,'North Gerdaborough'),(52,'80295478500020','825 Marcella Streets\nNorth Aidabury, WA 30715',979613335,'1992-05-26 02:27:02','1971-04-14 20:42:59',191,476,'SN',9,51.87,'1979-10-14 22:08:35','1986-09-01 03:18:01',58827,'Parisianbury');
/*!40000 ALTER TABLE `restaurant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(130) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname_user` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_visit_user` int NOT NULL,
  `bonus_reduc_user` int NOT NULL,
  `birthdate_user` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `last_visit_user` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `last_commande_user` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `create_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `update_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (25,'greenholt.max@romaguera.com','[\"ROLE_USER\"]','$2y$13$i0D1L.ngbWutIIvuT1VgV.DaQe2jsSEWpOnzplqUN0qPUOoIYGzoq','Mabel Huel II','Samson','8308 Bode Place Suite 434\nNitzschemouth, WY 76355-0562','Hansenburgh','(107)086-4290x354',51,13,'1970-04-09 07:25:43','1984-11-03 16:29:03',NULL,'1975-08-10 01:29:14',NULL,'Username0'),(26,'jolie93@hotmail.com','[\"ROLE_USER\"]','$2y$13$35SgQ9caVcCH3PCCweINrOnGOv/DEXWN/DODLchP7haZrSpBSFIs.','Cecil Pfannerstill DDS','Danielle','21006 Ruecker Summit\nWest Alta, OH 14524','New Braxton','(416)880-0322x53552',42,57,'1981-04-17 22:52:56','2002-12-19 17:48:25',NULL,'2016-07-27 06:21:09',NULL,'Username1'),(27,'cornell.dach@gmail.com','[\"ROLE_USER\"]','$2y$13$IiiL8qxMytilU8g1zbBB8OV2t23EexODLVA9JSsMnSO4jZ3H9qDAe','Wanda Hintz','Lisa','6059 Willms Summit\nKaileechester, MT 56353','Ryanmouth','684-808-6252x95116',18,30,'1978-10-03 12:29:23','2022-02-01 13:17:45',NULL,'1974-07-13 02:07:38',NULL,'Username2'),(28,'jarrod.keeling@hotmail.com','[\"ROLE_USER\"]','$2y$13$UUHYRHXYXOs8xcghM0axqOJxmTxxiaGNF.u.ehsTPkoHHJvQLAQ2O','Dr. Heidi Greenholt','Adrienne','2179 Koepp Station Suite 867\nLaurencehaven, CO 75795','Nienowburgh','047.314.6392',20,95,'2000-10-08 05:44:19','2007-04-08 02:56:24',NULL,'1989-07-17 11:50:07',NULL,'Username3'),(29,'berneice95@kuhlman.org','[\"ROLE_USER\"]','$2y$13$26p8gVQckYA6MkaRvP.mb.GBvuwm93kvYxdRYONtIXIH5edUTmH.a','Watson Lakin','Princess','0430 Winfield Route\nDavisberg, WA 38538','Towneshire','372-454-0841',40,83,'2002-04-24 04:34:53','1979-10-19 22:14:29',NULL,'1984-05-17 11:19:57',NULL,'Username4'),(30,'weimann.fatima@gmail.com','[\"ROLE_USER\"]','$2y$13$0a8IBkiASXYQ9fUCRAvjhORWNcuxxXIt3vt2V660tt1zkcWAu2QNK','Prof. Raoul Hoeger Sr.','Ibrahim','159 Mitchell Views Apt. 190\nBradfort, KY 20947','Port Stantonchester','168-082-3506',21,99,'1996-12-15 11:03:19','1988-01-28 04:11:01',NULL,'2015-06-02 02:49:16',NULL,'Username5'),(31,'qkertzmann@eichmann.com','[\"ROLE_USER\"]','$2y$13$Vicnu7Ei70YCId.c.cIeo.gWnc9qE0LnxHYNiHkc4qcfy5kopgot.','Dillan Toy Sr.','Cristal','08081 Ronny Fall Apt. 412\nNew Cliftonview, AR 19357','Havenchester','(697)360-6486x591',20,84,'1983-08-13 11:07:16','1992-02-07 07:59:52',NULL,'2016-09-15 03:33:17',NULL,'Username6'),(32,'friesen.jerod@yahoo.com','[\"ROLE_USER\"]','$2y$13$6eMtAjATqGKe7Q7XTBZ9XeMXxpV1wP0CDy.CRbFtVEfVlZJ9F9Qum','Dr. Dan Steuber DDS','Nelda','5366 Ford Locks\nWest Ewellbury, IN 85688-2116','Port Destinside','(168)731-9557',43,12,'2005-12-31 22:37:41','1986-07-04 08:11:20',NULL,'1993-12-12 14:38:11',NULL,'Username7'),(33,'kuhlman.prince@gmail.com','[\"ROLE_USER\"]','$2y$13$bSUfooAGep/FbxUgJ6/WWOUg1DvuEK40kT/b4e/hJZxt5Yf8Nwl/G','Mr. Wiley Donnelly III','Brionna','2753 Murray Fort\nEast Destinee, MI 25108','Lake Shakirastad','833.235.3638x90650',96,95,'2005-02-06 14:47:18','1995-04-17 02:50:25',NULL,'1998-12-02 19:33:52',NULL,'Username8'),(34,'kuhic.nona@hilpert.net','[\"ROLE_USER\"]','$2y$13$TUYNIXcvL8ZTKSu37puVZ.6vhmKYAgFxGDe4G0/EJge6Mv4POeg52','Mossie D\'Amore','Porter','501 Carter Port\nLednerborough, KY 39702','Braulioshire','+19(9)6119714165',84,83,'2020-06-10 06:10:54','1974-10-21 22:20:30',NULL,'1983-03-14 09:52:00',NULL,'Username9');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-11 21:51:16
