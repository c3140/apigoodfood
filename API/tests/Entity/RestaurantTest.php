<?php

namespace App\Tests\Entity;

use App\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RestaurantTest extends KernelTestCase
{
    public function getEntity(): Restaurant
    {
        return (new Restaurant)
                ->setNumSiretRest("123 568 941 00056")
                ->setAdresseRest("29 rue de la bouf")
                ->setNumPlaceRest(100)
                ->setTimeOpenRest(new \DateTimeImmutable("now"))
                ->setTimeCloseRest(new \DateTimeImmutable("now"))
                ->setNumberStaffRest(10)
                ->setNumberVisitRest(100000)
                ->setCountryRest("France")
                ->setCaRest(150350.67)
                ->setSatisfactionRateRest(97)
                ->setRanknumber(1)
                ->setCreateAt(new \DateTimeImmutable("now"))
                ->setUpdateAt(new \DateTimeImmutable("now"))
            ;
    }

    public function assertHasErrors(Restaurant $restaurant, int $number = 0): void
    {
        self::bootKernel(); 
        $errors = KernelTestCase::getContainer()->get(ValidatorInterface::class)->validate($restaurant);
        $this->assertCount($number, $errors);
    }

    public function propertyHasError($property, $expected): void
    {
        self::bootKernel();
        $this->assertSame($expected, $property);
    }

    public function testValidEntity(): void
    {
        $this->assertHasErrors($this->getEntity());
    }

    public function testValidPropertyNumSiretRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getNumSiretRest(), "123 568 941 00056");
    }

    public function testValidPropertyAdresseRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getAdresseRest(), "29 rue de la bouf");
    }

    public function testValidPropertyNumPLaceRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getNumPlaceRest(), 100);
    }

    public function testValidPropertyTimeOpenRestaurant(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getTimeOpenRest()->format("dd/MM/yyyy"), $date->format("dd/MM/yyyy"));
    }

    public function testValidPropertyTimeCLoseRestaurant(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getTimeCLoseRest()->format("dd/MM/yyyy"), $date->format("dd/MM/yyyy"));
    }

    public function testValidPropertyNumberStaffRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getNumberStaffRest(), 10);
    }

    public function testValidPropertyNumberVisitRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getNumberVisitRest(), 100000);
    }

    public function testValidPropertyCountryRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getCountryRest(), "France");
    }

    public function testValidPropertyCaRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getCaRest(), 150350.67);
    }

    public function testValidPropertySatisfactionRateRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getSatisfactionRateRest(), 97.0);
    }

    public function testValidPropertyRankNumberRestaurant(): void
    {
        $this->propertyHasError($this->getEntity()->getRanknumber(), 1);
    }

    public function testValidPropertyCreatedAtRestaurant(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getCreateAt()->format("dd/MM/yyyy"), $date->format("dd/MM/yyyy"));
    }

    public function testValidPropertyUpdateAtRestaurant(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getUpdateAt()->format("dd/MM/yyyy"), $date->format("dd/MM/yyyy"));
    }
}
