<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserTest extends KernelTestCase
{
    /*public function testSomething(): void
    {
        $this->assertTrue(true);
    }*/

    public function testValidEntity() {
        
        $user = (new User())
                ->setFirstnameUser("Antoine")
                ->setLastnameUser("Veriepe")
                ->setEmail("veriepeantoineV2@gmail.com")
                ->setRoles(["ROLE_USER"])
                ->setPassword("badPassword")
                ->setAddressUser("29 rue du dev")
                ->setCityUser("Lille")
                ->setNumPhone("0657853412")
                ->setNumberVisitUser(500)
                ->setBonusReducUser(20)
                ->setBirthdateUser(new \DateTimeImmutable("1999-08-02"))
                ->setLastVisitUser(new \DateTimeImmutable("2022-01-03"))
                ->setLastCommandeUser(new \DateTimeImmutable("2022-01-01"))
            ;

        self::bootKernel(); 
        $errors = KernelTestCase::getContainer()->get(ValidatorInterface::class)->validate($user);
        $this->assertCount(0, $errors);
    }
}
