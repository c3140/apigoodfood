<?php

namespace App\Tests\Entity;

use App\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MenuTest extends KernelTestCase
{

    public function getEntity(): Menu
    {
<<<<<<< HEAD:tests/Entity/MenuTest.php
        return (new Menu())
                ->setNameMenu("Menu B3")
                ->setPriceMenu(50)
                ->setTypeMenu("B3")
                ->setNumberRankMenu(3)
=======
        return (new Commande())
                ->setPriceGlobal(49.99)
                ->setValidCommande(true)
                ->setSatisfactionCommande(10)
                //->setNameMenu("Menu B3")
                //->setPriceMenu(50)
                //->setIdCountryMenu("3")
                //->setTypeMenu("B3")
                //->setNumberRankMenu("3")
>>>>>>> c933843fcb53b1ae698d0e0c5e8f21c26959df71:tests/Entity/CommandeTest.php
                ->setCreateAt(new \DateTimeImmutable('now'))
                ->setUpdateAt(new \DateTimeImmutable('now'))
        ;
    }

    public function assertHasErrors(Menu $menu, int $number = 0): void
    {
        self::bootKernel(); 
        $errors = KernelTestCase::getContainer()->get(ValidatorInterface::class)->validate($menu);
        $this->assertCount($number, $errors);
    }

    public function propertyHasError($property, $expected): void
    {
        self::bootKernel();
        $this->assertSame($expected, $property);
    }

    public function testValidEntity(): void
    {    
        $this->assertHasErrors($this->getEntity());   
    }

    public function testValidPropertyNameMenu(): void
    {
        $this->propertyHasError($this->getEntity()->getNameMenu(), "Menu B3");
    }

    public function testValidPropertyPriceMenu(): void
    {
        $this->propertyHasError($this->getEntity()->getPriceMenu(), 50.0);
    }

    public function testValidPropertyTypeMenu(): void
    {
        $this->propertyHasError($this->getEntity()->getTypeMenu(), "B3");
    }

    public function testValidPropertyNumberRankMenu(): void
    {
        $this->propertyHasError($this->getEntity()->getNumberRankMenu(), 3);
    }

    public function testValidPropertyCreatedAt(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getCreateAt()->format('dd/MM/yyyy'), $date->format('dd/MM/yyyy'));
    }

    public function testValidPropertyUpdatedAt(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getUpdateAt()->format('dd/MM/yyyy'), $date->format('dd/MM/yyyy'));
    }

    /* pour ce test rajouté des contrainte de validation dans les entité smfony
    public function testInvalidEntity()
    {
        $country = (new Country())
                    ->setNameCountry("Franceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
                    ->setNumberRestCountry(50.36)
                    ->setCaCountry(50000.50)
                    ->setCreateAt(new \DateTimeImmutable('now'))
                ;
        
        $this->assertHasErrors($country, 1);
    }*/
}