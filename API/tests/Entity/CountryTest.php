<?php

namespace App\Tests\Entity;

use App\Entity\Country;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CountryTest extends KernelTestCase
{

    public function getEntity(): Country
    {
        return (new Country())
                ->setNameCountry("France")
                ->setNumberRestCountry(50)
                ->setCaCountry(50000.50)
                ->setCreateAt(new \DateTimeImmutable('now'))
                ->setUpdateAt(new \DateTimeImmutable('now'))
        ;
    }

    public function assertHasErrors(Country $country, int $number = 0): void
    {
        self::bootKernel(); 
        $errors = KernelTestCase::getContainer()->get(ValidatorInterface::class)->validate($country);
        $this->assertCount($number, $errors);
    }

    public function propertyHasError($property, $expected): void
    {
        self::bootKernel();
        $this->assertSame($expected, $property);
    }

    public function testValidEntity(): void
    {    
        $this->assertHasErrors($this->getEntity());   
    }

    public function testValidPropertyNameCountry(): void
    {
        $this->propertyHasError($this->getEntity()->getNameCountry(), "France");
    }

    public function testValidPropertyNumberRestCountry(): void
    {
        $this->propertyHasError($this->getEntity()->getNumberRestCountry(), 50);
    }

    public function testValidPropertyCaCountry(): void
    {
        $this->propertyHasError($this->getEntity()->getCaCountry(), 50000.50);
    }

    public function testValidPropertyCreatedAt(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getCreateAt()->format('dd/MM/yyyy'), $date->format('dd/MM/yyyy'));
    }

    public function testValidPropertyUpdatedAt(): void
    {
        $date = new \DateTimeImmutable("now");
        $this->propertyHasError($this->getEntity()->getUpdateAt()->format('dd/MM/yyyy'), $date->format('dd/MM/yyyy'));
    }

    /* pour ce test rajouté des contrainte de validation dans les entité smfony
    public function testInvalidEntity()
    {
        $country = (new Country())
                    ->setNameCountry("Franceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
                    ->setNumberRestCountry(50.36)
                    ->setCaCountry(50000.50)
                    ->setCreateAt(new \DateTimeImmutable('now'))
                ;
        
        $this->assertHasErrors($country, 1);
    }*/
}
