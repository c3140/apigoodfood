<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220409134513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE rank_menu');
        $this->addSql('DROP TABLE rank_restaurant');
        $this->addSql('ALTER TABLE menu ADD number_rank_menu INT NOT NULL');
        $this->addSql('ALTER TABLE restaurant ADD ca_rest DOUBLE PRECISION NOT NULL, CHANGE turnover_rest ranknumber INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rank_menu (id INT AUTO_INCREMENT NOT NULL, id_menu_id INT NOT NULL, number_rank_menu INT NOT NULL, name_menu VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, price_menu NUMERIC(10, 0) NOT NULL, UNIQUE INDEX UNIQ_7BA52A6C124A4062 (id_menu_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE rank_restaurant (id INT AUTO_INCREMENT NOT NULL, id_restaurant_id INT NOT NULL, ranknumber INT NOT NULL, adresse_restaurant VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, turnover_rest NUMERIC(20, 5) NOT NULL, country_rest VARCHAR(3) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, satisfaction_rate_rest NUMERIC(10, 0) NOT NULL, UNIQUE INDEX UNIQ_26574BC6FCFA10B (id_restaurant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE rank_menu ADD CONSTRAINT FK_7BA52A6C124A4062 FOREIGN KEY (id_menu_id) REFERENCES menu (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE rank_restaurant ADD CONSTRAINT FK_26574BC6FCFA10B FOREIGN KEY (id_restaurant_id) REFERENCES restaurant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE country CHANGE name_country name_country VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE menu DROP number_rank_menu, CHANGE name_menu name_menu VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE id_country_menu id_country_menu VARCHAR(3) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE type_menu type_menu VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE restaurant DROP ca_rest, CHANGE num_siret_rest num_siret_rest VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE adresse_rest adresse_rest VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE country_rest country_rest VARCHAR(3) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE ranknumber turnover_rest INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(130) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE lastname_user lastname_user VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE firstname_user firstname_user VARCHAR(30) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address_user address_user VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE city_user city_user VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE num_phone num_phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
