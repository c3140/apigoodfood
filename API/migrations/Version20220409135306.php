<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220409135306 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D053A933AB3E0E4 ON menu (name_menu)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EB95123F40783D37 ON restaurant (num_siret_rest)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country CHANGE name_country name_country VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_7D053A933AB3E0E4 ON menu');
        $this->addSql('ALTER TABLE menu CHANGE name_menu name_menu VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE id_country_menu id_country_menu VARCHAR(3) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE type_menu type_menu VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX UNIQ_EB95123F40783D37 ON restaurant');
        $this->addSql('ALTER TABLE restaurant CHANGE num_siret_rest num_siret_rest VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE adresse_rest adresse_rest VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE country_rest country_rest VARCHAR(3) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(130) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE lastname_user lastname_user VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE firstname_user firstname_user VARCHAR(30) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address_user address_user VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE city_user city_user VARCHAR(50) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE num_phone num_phone VARCHAR(20) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
