<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220308102402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commande (id INT AUTO_INCREMENT NOT NULL, num_siret_rest_id INT DEFAULT NULL, reservation_id INT DEFAULT NULL, price_global DOUBLE PRECISION NOT NULL, valid_commande TINYINT(1) NOT NULL, satisfaction_commande INT DEFAULT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_6EEAA67D735387D5 (num_siret_rest_id), INDEX IDX_6EEAA67DB83297E7 (reservation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name_country VARCHAR(255) NOT NULL, number_rest_country INT NOT NULL, ca_country DOUBLE PRECISION NOT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE listcommandes (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, commande_id INT DEFAULT NULL, menu_id INT DEFAULT NULL, restaurant_id INT DEFAULT NULL, reservation_id INT DEFAULT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_65795E2CA76ED395 (user_id), INDEX IDX_65795E2C82EA2E54 (commande_id), INDEX IDX_65795E2CCCD7E912 (menu_id), INDEX IDX_65795E2CB1E7706E (restaurant_id), INDEX IDX_65795E2CB83297E7 (reservation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, name_menu VARCHAR(255) NOT NULL, price_menu DOUBLE PRECISION NOT NULL, id_country_menu VARCHAR(3) NOT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', type_menu VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, number_customers INT NOT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL, num_siret_rest INT NOT NULL, adresse_rest VARCHAR(255) NOT NULL, num_place_rest INT NOT NULL, time_open_rest DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', time_close_rest DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', number_staff_rest INT NOT NULL, number_visit_rest INT NOT NULL, country_rest VARCHAR(255) NOT NULL, turnover_rest INT NOT NULL, satisfaction_rate_rest DOUBLE PRECISION NOT NULL, create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(130) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, lastname_user VARCHAR(50) NOT NULL, firstname_user VARCHAR(30) NOT NULL, address_user VARCHAR(255) DEFAULT NULL, city_user VARCHAR(50) DEFAULT NULL, num_phone VARCHAR(20) DEFAULT NULL, number_visit_user INT NOT NULL, bonus_reduc_user INT NOT NULL, birthdate_user DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_visit_user DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_commande_user DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', update_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commande ADD CONSTRAINT FK_6EEAA67D735387D5 FOREIGN KEY (num_siret_rest_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE commande ADD CONSTRAINT FK_6EEAA67DB83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE listcommandes ADD CONSTRAINT FK_65795E2CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE listcommandes ADD CONSTRAINT FK_65795E2C82EA2E54 FOREIGN KEY (commande_id) REFERENCES commande (id)');
        $this->addSql('ALTER TABLE listcommandes ADD CONSTRAINT FK_65795E2CCCD7E912 FOREIGN KEY (menu_id) REFERENCES menu (id)');
        $this->addSql('ALTER TABLE listcommandes ADD CONSTRAINT FK_65795E2CB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id)');
        $this->addSql('ALTER TABLE listcommandes ADD CONSTRAINT FK_65795E2CB83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE listcommandes DROP FOREIGN KEY FK_65795E2C82EA2E54');
        $this->addSql('ALTER TABLE listcommandes DROP FOREIGN KEY FK_65795E2CCCD7E912');
        $this->addSql('ALTER TABLE commande DROP FOREIGN KEY FK_6EEAA67DB83297E7');
        $this->addSql('ALTER TABLE listcommandes DROP FOREIGN KEY FK_65795E2CB83297E7');
        $this->addSql('ALTER TABLE commande DROP FOREIGN KEY FK_6EEAA67D735387D5');
        $this->addSql('ALTER TABLE listcommandes DROP FOREIGN KEY FK_65795E2CB1E7706E');
        $this->addSql('ALTER TABLE listcommandes DROP FOREIGN KEY FK_65795E2CA76ED395');
        $this->addSql('DROP TABLE commande');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE listcommandes');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE user');
    }
}
