<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Menu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MenuFixtures extends Fixture
{
    private ObjectManager $manager;

    private \Faker\Generator $faker;

     /**
     * @var array<string>
     */
    private array $menus = [
        'MENU 1',
        'MENU 2',
        'MENU 3',
        'MENU 4',
        'MENU 5',
        'MENU 6',
        'MENU 7',
        'MENU 8',
        'MENU 9',
        'MENU 10'
    ];

    /**
     * @var array<string>
     */
    private array $typeMenus = [
        'ITALIEN',
        'FRANCAIS',
        'ASIATIQUE',
        'POISSON'
    ];

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();

        $this->generateMenu(10);

        $manager->flush();
    }

    /**
     * Generate Menu entity
     * @param int $number
     */
    private function generateMenu(int $number): void
    {
        for($i = 0; $i < $number; $i++)
        {
            $menu = new Menu();

            //$menuName = ;

            $menu->setNameMenu($this->menus[$i])
                 ->setPriceMenu($this->faker->randomFloat(2, 1, 100))
                 ->setIdCountryMenu($this->faker->countryCode())
                 ->setCreateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                 ->setUpdateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                 ->setTypeMenu($this->faker->randomElement($this->typeMenus))
                 ->setNumberRankMenu($i)
                ;

            $this->manager->persist($menu);
        }
    }
}
