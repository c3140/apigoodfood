<?php

namespace App\DataFixtures;

use App\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RestaurantFixtures extends Fixture
{
    private ObjectManager $manager;
    private \Faker\Generator $faker;

    private array $SIRETS = [
        '80295478500021',
        '80295478500022',
        '80295478500023',
        '80295478500024',
        '80295478500025',
        '80295478500026',
        '80295478500027',
        '80295478500028',
        '80295478500029',
        '80295478500020'
    ];
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr');
        //$this->faker = Factory::create();
        //$this->faker->locale('fr_FR');

        $this->generateRestaurants(10);

        $manager->flush();
    }

    /**
     * Generate Restaurants
     * @param int $numbers
     */
    private function generateRestaurants(int $numbers): void
    {
        for($i = 0; $i < $numbers; $i++)
        {
            $restaurant = new Restaurant();
            //$this->faker->siret()
            $restaurant->setNumSiretRest($this->SIRETS[$i])
                       ->setAdresseRest($this->faker->address())
                       ->setNumPlaceRest($this->faker->randomNumber())
                       ->setTimeOpenRest(\DateTimeImmutable::createFromMutable($this->faker->dateTime())) //jsp
                       ->setTimeCloseRest(\DateTimeImmutable::createFromMutable($this->faker->dateTime())) //jsp
                       ->setNumberStaffRest($this->faker->randomNumber())
                       ->setNumberVisitRest($this->faker->randomNumber())
                       ->setCountryRest($this->faker->countryCode())
                       ->setCaRest($this->faker->randomNumber())
                       ->setSatisfactionRateRest($this->faker->randomFloat(2, 0, 100))
                       ->setRanknumber($i)
                       ->setCity($this->faker->city)
                       ->setCreateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                       ->setUpdateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                    ;

            $this->manager->persist($restaurant);
        }
    }
}