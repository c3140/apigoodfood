<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private \Faker\Generator $faker;

    private ObjectManager $manager;

    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();

        $this->GenerateUser(10);

        $manager->flush();
    }

    public function GenerateUser(int $number)
    {
        for($i = 0; $i < $number; $i++)
        {
            $user = new User();
            $user->setEmail($this->faker->email())
                 ->setUsername("Username{$i}")
                 ->setRoles(["ROLE_USER"])
                 ->setPassword($this->hasher->hashPassword($user, "badPassword"))
                 ->setLastnameUser($this->faker->name())
                 ->setFirstnameUser($this->faker->firstName())
                 ->setAddressUser($this->faker->address())
                 ->setCityUser($this->faker->city())
                 ->setNumPhone($this->faker->phoneNumber())
                 ->setNumberVisitUser(rand(0, 100))
                 ->setBonusReducUser(rand(0, 100))
                 ->setBirthdateUser(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                 ->setLastVisitUser(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                 ->setCreateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                ;

            $this->addReference("user{$i}", $user);

            $this->manager->persist($user);
        }
    }
}
