<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Country;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class CountryFixtures extends Fixture
{
    private ObjectManager $manager;
    private \Faker\Generator $faker;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        
        $this->generateCountry(10);

        $manager->flush();
    }

    /**
     * Generate COuntry entity
     * @param int $numbers
     */
    private function generateCountry(int $numbers): void
    {
        for($i = 0; $i < $numbers; $i++)
        {
            $country = new Country();

            $country->setNameCountry($this->faker->country())
                    ->setNumberRestCountry($this->faker->randomNumber())
                    ->setCaCountry($this->faker->randomFloat())
                    ->setCreateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                    ->setUpdateAt(\DateTimeImmutable::createFromMutable($this->faker->dateTime()))
                ;
            
            $this->manager->persist($country);
        }
    }
}
