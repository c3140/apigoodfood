<?php

namespace App\DataPersister;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserDataPersister implements DataPersisterInterface
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $hasher;

    public function __construct(EntityManagerINterface $entityManager, UserPasswordHasherInterface $hasher)
    {
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;
    }

    public function supports($data): bool
    {
        return $data instanceof User;   
    }

    /**
     * @param User $data
     */
    public function persist($data)
    {
        if($data->getPassword())
        {
            $data->setPassword($this->hasher->hashPassword($data, $data->getPassword()));
            $data->eraseCredentials();
        }

        $data->customSetUpdateAt(new \DateTimeImmutable('now'));

        $data->setRoles(["ROLE_USER"]);

        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}