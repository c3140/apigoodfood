<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource()]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: Commande::class)]
    private $id_commande;

    #[ORM\Column(type: 'integer')]
    private $number_customers;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: Listcommandes::class)]
    private $listcommandes;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $username;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $adresseRestaurant;

    #[ORM\Column(type: 'datetime_immutable')]
    private $dateReservationAt;

    public function __construct()
    {
        $this->id_commande = new ArrayCollection();
        $this->listcommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Commande>
     */
    public function getIdCommande(): Collection
    {
        return $this->id_commande;
    }

    public function addIdCommande(Commande $idCommande): self
    {
        if (!$this->id_commande->contains($idCommande)) {
            $this->id_commande[] = $idCommande;
            $idCommande->setReservation($this);
        }

        return $this;
    }

    public function removeIdCommande(Commande $idCommande): self
    {
        if ($this->id_commande->removeElement($idCommande)) {
            // set the owning side to null (unless already changed)
            if ($idCommande->getReservation() === $this) {
                $idCommande->setReservation(null);
            }
        }

        return $this;
    }

    public function getNumberCustomers(): ?int
    {
        return $this->number_customers;
    }

    public function setNumberCustomers(int $number_customers): self
    {
        $this->number_customers = $number_customers;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    /**
     * @return Collection<int, Listcommandes>
     */
    public function getListcommandes(): Collection
    {
        return $this->listcommandes;
    }

    public function addListcommande(Listcommandes $listcommande): self
    {
        if (!$this->listcommandes->contains($listcommande)) {
            $this->listcommandes[] = $listcommande;
            $listcommande->setReservation($this);
        }

        return $this;
    }

    public function removeListcommande(Listcommandes $listcommande): self
    {
        if ($this->listcommandes->removeElement($listcommande)) {
            // set the owning side to null (unless already changed)
            if ($listcommande->getReservation() === $this) {
                $listcommande->setReservation(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAdresseRestaurant(): ?string
    {
        return $this->adresseRestaurant;
    }

    public function setAdresseRestaurant(?string $adresseRestaurant): self
    {
        $this->adresseRestaurant = $adresseRestaurant;

        return $this;
    }

    public function getDateReservationAt(): ?\DateTimeImmutable
    {
        return $this->dateReservationAt;
    }

    public function setDateReservationAt(\DateTimeImmutable $dateReservationAt): self
    {
        $this->dateReservationAt = $dateReservationAt;

        return $this;
    }
}
