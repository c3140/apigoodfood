<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CommandeRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: CommandeRepository::class)]
#[ApiResource()]
class Commande
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $price_global;

    #[ORM\ManyToOne(targetEntity: Restaurant::class, inversedBy: 'commandes')]
    private $num_siret_rest;

    #[ORM\Column(type: 'boolean')]
    private $valid_commande;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $satisfaction_commande;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    #[ORM\ManyToOne(targetEntity: Reservation::class, inversedBy: 'id_commande')]
    private $reservation;

    #[ORM\OneToMany(mappedBy: 'commande', targetEntity: Listcommandes::class)]
    private $listcommandes;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $username;

    public function __construct()
    {
        $this->listcommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPriceGlobal(): ?float
    {
        return $this->price_global;
    }

    public function setPriceGlobal(float $price_global): self
    {
        $this->price_global = $price_global;

        return $this;
    }

    public function getNumSiretRest(): ?Restaurant
    {
        return $this->num_siret_rest;
    }

    public function setNumSiretRest(?Restaurant $num_siret_rest): self
    {
        $this->num_siret_rest = $num_siret_rest;

        return $this;
    }

    public function getValidCommande(): ?bool
    {
        return $this->valid_commande;
    }

    public function setValidCommande(bool $valid_commande): self
    {
        $this->valid_commande = $valid_commande;

        return $this;
    }

    public function getSatisfactionCommande(): ?int
    {
        return $this->satisfaction_commande;
    }

    public function setSatisfactionCommande(?int $satisfaction_commande): self
    {
        $this->satisfaction_commande = $satisfaction_commande;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * @return Collection<int, Listcommandes>
     */
    public function getListcommandes(): Collection
    {
        return $this->listcommandes;
    }

    public function addListcommande(Listcommandes $listcommande): self
    {
        if (!$this->listcommandes->contains($listcommande)) {
            $this->listcommandes[] = $listcommande;
            $listcommande->setCommande($this);
        }

        return $this;
    }

    public function removeListcommande(Listcommandes $listcommande): self
    {
        if ($this->listcommandes->removeElement($listcommande)) {
            // set the owning side to null (unless already changed)
            if ($listcommande->getCommande() === $this) {
                $listcommande->setCommande(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }
}
