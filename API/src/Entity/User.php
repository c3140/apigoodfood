<?php

namespace App\Entity;

//use Carbon\Carbon;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Filesystem\Path;
use ApiPlatform\Core\Action\NotFoundAction;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
/*#[ApiResource(
    collectionOperations:["get", "post"],
    itemOperations:["get"=> ["path" => 'getUtilisateur/{id}'], "put"],
    shortName:"utilisateur",
    normalizationContext: ["groups"=> ["user:read"]],
    denormalizationContext: ["groups" => ["user:write"]]
)]*/
#[ApiResource()]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: false)]
    //#[Groups(["read"])]
    private $id;

    #[ORM\Column(type: 'string', length: 130, unique: true)]
    //#[Groups(["read", "write"])]
    private $email;

    //#[Groups(["read", "write"])]
    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[ORM\Column(type: 'string', length: 50)]
    private $lastname_user;

    #[ORM\Column(type: 'string', length: 30)]
    private $firstname_user;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $address_user;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $city_user;

    #[ORM\Column(type: 'string', length: 20, nullable: true)]
    private $num_phone;

    #[ORM\Column(type: 'integer')]
    private $number_visit_user;

    #[ORM\Column(type: 'integer')]
    private $bonus_reduc_user;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $birthdate_user;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $last_visit_user;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $last_commande_user;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Listcommandes::class)]
    private $listcommandes;

    #[ORM\Column(type: 'string', length: 255)]
    #[ApiProperty(identifier: true)]
    private $username;

    public function __construct()
    {
        $this->listcommandes = new ArrayCollection();
        $this->create_at = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastnameUser(): ?string
    {
        return $this->lastname_user;
    }

    public function setLastnameUser(string $lastname_user): self
    {
        $this->lastname_user = $lastname_user;

        return $this;
    }

    public function getFirstnameUser(): ?string
    {
        return $this->firstname_user;
    }

    public function setFirstnameUser(string $firstname_user): self
    {
        $this->firstname_user = $firstname_user;

        return $this;
    }

    public function getAddressUser(): ?string
    {
        return $this->address_user;
    }

    public function setAddressUser(string $address_user): self
    {
        $this->address_user = $address_user;

        return $this;
    }

    public function getCityUser(): ?string
    {
        return $this->city_user;
    }

    public function setCityUser(string $city_user): self
    {
        $this->city_user = $city_user;

        return $this;
    }

    public function getNumPhone(): ?string
    {
        return $this->num_phone;
    }

    public function setNumPhone(string $num_phone): self
    {
        $this->num_phone = $num_phone;

        return $this;
    }

    public function getNumberVisitUser(): ?int
    {
        return $this->number_visit_user;
    }

    public function setNumberVisitUser(int $number_visit_user): self
    {
        $this->number_visit_user = $number_visit_user;

        return $this;
    }

    public function getBonusReducUser(): ?int
    {
        return $this->bonus_reduc_user;
    }

    public function setBonusReducUser(int $bonus_reduc_user): self
    {
        $this->bonus_reduc_user = $bonus_reduc_user;

        return $this;
    }

    public function getBirthdateUser(): ?\DateTimeImmutable
    {
        return $this->birthdate_user;
    }

    public function setBirthdateUser(\DateTimeImmutable $birthdate_user): self
    {
        $this->birthdate_user = $birthdate_user;

        return $this;
    }

    public function getLastVisitUser(): ?\DateTimeImmutable
    {
        return $this->last_visit_user;
    }

    public function setLastVisitUser(\DateTimeImmutable $last_visit_user): self
    {
        $this->last_visit_user = $last_visit_user;

        return $this;
    }

    public function getLastCommandeUser(): ?\DateTimeImmutable
    {
        return $this->last_commande_user;
    }

    public function setLastCommandeUser(\DateTimeImmutable $last_commande_user): self
    {
        $this->last_commande_user = $last_commande_user;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function getCreateAtAgo(): string
    {
        //return $this->create_at;
        return Carbon::instance($this->create_at)->diffForHumans();
        
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function customSetUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    /**
     * @return Collection<int, Listcommandes>
     */
    public function getListcommandes(): Collection
    {
        return $this->listcommandes;
    }

    public function addListcommande(Listcommandes $listcommande): self
    {
        if (!$this->listcommandes->contains($listcommande)) {
            $this->listcommandes[] = $listcommande;
            $listcommande->setUser($this);
        }

        return $this;
    }

    public function removeListcommande(Listcommandes $listcommande): self
    {
        if ($this->listcommandes->removeElement($listcommande)) {
            // set the owning side to null (unless already changed)
            if ($listcommande->getUser() === $this) {
                $listcommande->setUser(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }
}
