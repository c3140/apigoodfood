<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CountryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
#[ApiResource()]
class Country
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name_country;

    #[ORM\Column(type: 'integer')]
    private $number_rest_country;

    #[ORM\Column(type: 'float')]
    private $ca_country;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCountry(): ?string
    {
        return $this->name_country;
    }

    public function setNameCountry(string $name_country): self
    {
        $this->name_country = $name_country;

        return $this;
    }

    public function getNumberRestCountry(): ?int
    {
        return $this->number_rest_country;
    }

    public function setNumberRestCountry(int $number_rest_country): self
    {
        $this->number_rest_country = $number_rest_country;

        return $this;
    }

    public function getCaCountry(): ?float
    {
        return $this->ca_country;
    }

    public function setCaCountry(float $ca_country): self
    {
        $this->ca_country = $ca_country;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }
}
