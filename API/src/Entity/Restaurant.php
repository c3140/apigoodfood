<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RestaurantRepository::class)]
#[ApiResource()]
class Restaurant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', unique: true)]
    private $num_siret_rest;

    #[ORM\Column(type: 'string', length: 255)]
    private $adresse_rest;

    #[ORM\Column(type: 'integer')]
    private $num_place_rest;

    #[ORM\Column(type: 'datetime_immutable')]
    private $time_open_rest;

    #[ORM\Column(type: 'datetime_immutable')]
    private $time_close_rest;

    #[ORM\Column(type: 'integer')]
    private $number_staff_rest;

    #[ORM\Column(type: 'integer')]
    private $number_visit_rest;

    #[ORM\Column(type: 'string', length: 3)]
    private $country_rest;

    #[ORM\Column(type: 'float')]
    private $ca_rest;

    #[ORM\Column(type: 'float')]
    private $satisfaction_rate_rest;

    #[ORM\Column(type: 'integer')]
    private $Ranknumber;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    #[ORM\OneToMany(mappedBy: 'num_siret_rest', targetEntity: Commande::class)]
    private $commandes;

    #[ORM\OneToMany(mappedBy: 'restaurant', targetEntity: Listcommandes::class)]
    private $listcommandes;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
        $this->listcommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumSiretRest(): ?string
    {
        return $this->num_siret_rest;
    }

    public function setNumSiretRest(string $num_siret_rest): self
    {
        $this->num_siret_rest = $num_siret_rest;

        return $this;
    }

    public function getAdresseRest(): ?string
    {
        return $this->adresse_rest;
    }

    public function setAdresseRest(string $adresse_rest): self
    {
        $this->adresse_rest = $adresse_rest;

        return $this;
    }

    public function getNumPlaceRest(): ?int
    {
        return $this->num_place_rest;
    }

    public function setNumPlaceRest(int $num_place_rest): self
    {
        $this->num_place_rest = $num_place_rest;

        return $this;
    }

    public function getTimeOpenRest(): ?\DateTimeImmutable
    {
        return $this->time_open_rest;
    }

    public function setTimeOpenRest(\DateTimeImmutable $time_open_rest): self
    {
        $this->time_open_rest = $time_open_rest;

        return $this;
    }

    public function getTimeCloseRest(): ?\DateTimeImmutable
    {
        return $this->time_close_rest;
    }

    public function setTimeCloseRest(\DateTimeImmutable $time_close_rest): self
    {
        $this->time_close_rest = $time_close_rest;

        return $this;
    }

    public function getNumberStaffRest(): ?int
    {
        return $this->number_staff_rest;
    }

    public function setNumberStaffRest(int $number_staff_rest): self
    {
        $this->number_staff_rest = $number_staff_rest;

        return $this;
    }

    public function getNumberVisitRest(): ?int
    {
        return $this->number_visit_rest;
    }

    public function setNumberVisitRest(int $number_visit_rest): self
    {
        $this->number_visit_rest = $number_visit_rest;

        return $this;
    }

    public function getCountryRest(): ?string
    {
        return $this->country_rest;
    }

    public function setCountryRest(string $country_rest): self
    {
        $this->country_rest = $country_rest;

        return $this;
    }

    public function getCaRest(): ?float
    {
        return $this->ca_rest;
    }

    public function setCaRest(float $ca_rest): self
    {
        $this->ca_rest = $ca_rest;

        return $this;
    }

    public function getSatisfactionRateRest(): ?float
    {
        return $this->satisfaction_rate_rest;
    }

    public function setSatisfactionRateRest(float $satisfaction_rate_rest): self
    {
        $this->satisfaction_rate_rest = $satisfaction_rate_rest;

        return $this;
    }

    public function getRanknumber(): ?int
    {
        return $this->Ranknumber;
    }

    public function setRanknumber(int $Ranknumber): self
    {
        $this->Ranknumber = $Ranknumber;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    /**
     * @return Collection<int, Commande>
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setNumSiretRest($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getNumSiretRest() === $this) {
                $commande->setNumSiretRest(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Listcommandes>
     */
    public function getListcommandes(): Collection
    {
        return $this->listcommandes;
    }

    public function addListcommande(Listcommandes $listcommande): self
    {
        if (!$this->listcommandes->contains($listcommande)) {
            $this->listcommandes[] = $listcommande;
            $listcommande->setRestaurant($this);
        }

        return $this;
    }

    public function removeListcommande(Listcommandes $listcommande): self
    {
        if ($this->listcommandes->removeElement($listcommande)) {
            // set the owning side to null (unless already changed)
            if ($listcommande->getRestaurant() === $this) {
                $listcommande->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }
}
