<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MenuRepository::class)]
#[ApiResource()]
class Menu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private $name_menu;

    #[ORM\Column(type: 'float')]
    private $price_menu;

    #[ORM\Column(type: 'string', length: 3)]
    private $id_country_menu;

    #[ORM\Column(type: 'datetime_immutable')]
    private $create_at;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $update_at;

    #[ORM\Column(type: 'string', length: 255)]
    private $type_menu;

    #[ORM\Column(type: 'integer')]
    private $numberRankMenu;

    #[ORM\OneToMany(mappedBy: 'menu', targetEntity: Listcommandes::class)]
    private $listcommandes;

    public function __construct()
    {
        $this->listcommandes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameMenu(): ?string
    {
        return $this->name_menu;
    }

    public function setNameMenu(string $name_menu): self
    {
        $this->name_menu = $name_menu;

        return $this;
    }

    public function getPriceMenu(): ?float
    {
        return $this->price_menu;
    }

    public function setPriceMenu(float $price_menu): self
    {
        $this->price_menu = $price_menu;

        return $this;
    }

    public function getIdCountryMenu(): ?string
    {
        return $this->id_country_menu;
    }

    public function setIdCountryMenu(string $id_country_menu): self
    {
        $this->id_country_menu = $id_country_menu;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeImmutable $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeImmutable $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getTypeMenu(): ?string
    {
        return $this->type_menu;
    }

    public function setTypeMenu(string $type_menu): self
    {
        $this->type_menu = $type_menu;

        return $this;
    }

    public function getNumberRankMenu(): ?int
    {
        return $this->numberRankMenu;
    }

    public function setNumberRankMenu(int $numberRankMenu): self
    {
        $this->numberRankMenu = $numberRankMenu;

        return $this;
    }

    /**
     * @return Collection<int, Listcommandes>
     */
    public function getListcommandes(): Collection
    {
        return $this->listcommandes;
    }

    public function addListcommande(Listcommandes $listcommande): self
    {
        if (!$this->listcommandes->contains($listcommande)) {
            $this->listcommandes[] = $listcommande;
            $listcommande->setMenu($this);
        }

        return $this;
    }

    public function removeListcommande(Listcommandes $listcommande): self
    {
        if ($this->listcommandes->removeElement($listcommande)) {
            // set the owning side to null (unless already changed)
            if ($listcommande->getMenu() === $this) {
                $listcommande->setMenu(null);
            }
        }

        return $this;
    }
}
